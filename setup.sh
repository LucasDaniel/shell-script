#!/bin/bash

if which java > /dev/null ; then
    echo "Java is already installed"
    sleep 3    
else
    sudo add-apt-repository ppa:webupd8team/java
    sudo apt-get update
    sudo apt-get install oracle-java8-installer
    clear
    java -version
    sleep 3    
    clear
    sudo apt-get install oracle-java8-set-default
fi

clear

if which git > /dev/null ; then
    echo "Git is already installed"
    sleep 3
else
    sudo apt-get install git
    clear
    git --version
    sleep 3    
    clear
fi
clear


if which mvn > /dev/null ; then
    echo "Maven is already installed"
    sleep 3
else
    sudo apt-get install maven
    clear
    mvn --version
    sleep 3    
    clear
fi
clear

if which mysql > /dev/null ; then
    echo "mysql is already installed"
    sleep 3
else
    sudo apt-get install mysql-server -y
    clear
    mysql --version
    sleep 3    
    clear
fi
clear

if which mysql-workbench > /dev/null ; then
    echo "mysql workbench is already installed"
    sleep 3
else
    sudo apt-get install mysql-workbench -y
    clear
    mysql-workbench --version
    sleep 3    
    clear
fi
clear

if [ -d "/usr/local/netbeans-8.0.2/" ] ; then
    echo "netbeans is already installed"
    sleep 3
else
    if [ -f "netbeans-8.0.2-linux.sh" ]  ; then
        sudo sh netbeans-8.0.2-linux.sh
        rm netbeans-8.0.2-linux.sh
        clear
        /user/local/netbeans-8.0.2/bin/netbeans --version
        sleep 3    
        clear
    else
        wget http://download.netbeans.org/netbeans/8.0.2/final/bundles/netbeans-8.0.2-linux.sh
        sudo sh netbeans-8.0.2-linux.sh
        rm netbeans-8.0.2-linux.sh
        clear
        /user/local/netbeans-8.0.2/bin/netbeans --version
        sleep 3    
        clear
    fi
fi
clear


if [ -d "/home/$USER/Programas/activator-1.3.6-minimal" ] ; then
    echo "Play is already installed"
    sleep 3
else
    if [ -f "typesafe-activator-1.3.6-minimal.zip" ]  ; then
        unzip typesafe-activator-1.3.6-minimal.zip
	mv activator-1.3.6-minimal /home/$USER/Programas
        rm typesafe-activator-1.3.6-minimal.zip

        clear
        /user/local/netbeans-8.0.2/bin/netbeans --version
        sleep 3    
        clear
    else
        wget https://downloads.typesafe.com/typesafe-activator/1.3.6/typesafe-activator-1.3.6-minimal.zip
        sudo sh netbeans-8.0.2-linux.sh
        rm netbeans-8.0.2-linux.sh
        clear
        /user/local/netbeans-8.0.2/bin/netbeans --version
        sleep 3    
        clear
    fi
fi
clear










